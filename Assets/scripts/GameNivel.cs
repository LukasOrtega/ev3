﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameNivel : MonoBehaviour
{
    public InputField inputField;
    
    public void getdata()
    {
        string cadena = "";

        cadena = inputField.text;
        
        Debug.Log(cadena);

        PlayerPrefs.SetString("nombre", cadena);
    }

    public void Facil()
    {
        SceneManager.LoadScene("2- Escenario");
    }

    

    public void VolverMenu()
    {
        SceneManager.LoadScene("1- Menu");
    }
}