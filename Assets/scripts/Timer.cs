﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text TimerText;
    public float tiempo = 0.0f;

    public void Update()
    {
        tiempo = tiempo - 1 * Time.deltaTime;
        TimerText.text = "" + tiempo.ToString("f0");

        if(tiempo < 0)
        {
            SceneManager.LoadScene("GameOver");
            Cursor.lockState = CursorLockMode.None;
        }

    }

   
}
